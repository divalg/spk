<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/setup', 'SetupController')->only('edit', 'update');

Route::resource('/akreditasi', 'RankAkreditasiController');
Route::resource('/dosen', 'RankDosenController');
Route::resource('/fasilitas', 'RankFasilitasController');
Route::resource('/lokasi', 'RankLokasiController');
Route::resource('/universitas', 'UniversitasController');

Route::resource('/proses', 'SpkController')->only('index', 'store');
Route::resource('/history', 'HistoryController')->only('index');
