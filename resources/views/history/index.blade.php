@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
	    	        <h3>Riwayat Penggunaan SPK</h3>
                </div>

                <div class="card-body">

                    <table id="datatable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="text-align: center;"><b>No.</b></td>
                                <td style="text-align: center;"><b>Tanggal</b></td>
                                <td style="text-align: center;"><b>Email </b></td>
                                <td style="text-align: center;"><b>Pilihan<br>Akreditasi</b></td>
                                <td style="text-align: center;"><b>Pilihan<br>Dosen</b></td>
                                <td style="text-align: center;"><b>Pilihan<br>Fasilitas</b></td>
                                <td style="text-align: center;"><b>Pilihan<br>Lokasi</b></td>
                                <td style="text-align: center;"><b>Rekomendasi</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; ?>
                        @foreach ($data as $k=>$v)
                            <tr>
                                <td style="text-align: left;">{{ $no++ }}</td>
                                <td style="text-align: left;">{{ date('d M Y', strtotime($v->created_at)) }}</td>
                                <td style="text-align: left;">{{ $v->email }}</td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->akreditasi->keterangan) }}
                                </td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->dosen->keterangan) }}
                                </td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->fasilitas->keterangan) }}
                                </td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->lokasi->keterangan) }}
                                </td>
                                <td style="text-align: center;">{{ $v->universitas->nama }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
