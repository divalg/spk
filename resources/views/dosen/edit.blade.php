@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h3>Rank Dosen - Edit Data</h3>
            <div class="bootstrap-iso">
                <a href="{{ URL::to('/dosen') }}" class="btn btn-default">Kembali</a>
            </div>
            <hr>
            <form class="form-horizontal" method="POST" action="{{ url('dosen', $data->id) }}">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PATCH">

                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-12">
                            <input class="form-control" name="keterangan" placeholder="Keterangan" value="{{ $data->keterangan }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Grade</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="grade" placeholder="A / B / C" value="{{ $data->grade }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Value/Rank</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="number" min="1" name="rank" placeholder="Value/Rank" value="{{ $data->rank }}">
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-success">Update Data</button>
                </div>

            </form>
            <hr>
        </div>
    </div>
</div>

<script type="text/javascript">

	function cek()
	{
		let a = confirm("Rubah Data?");
        if(a) {
            $('#simpan').click();
        }
	}

</script>

@endsection
