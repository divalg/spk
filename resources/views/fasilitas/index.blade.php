@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
	    	        <h3>Ranking Fasilitas</h3>
                </div>

                <div class="card-body">

                    <a class="btn btn-primary btn-md" onclick='createItem()'>Input Baru</a>
                    <hr>
                    <table id="datatable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="text-align: center;"><b>Keterangan</b></td>
                                <td style="text-align: center;"><b>Grade </b></td>
                                <td style="text-align: center;"><b>Value/Rank </b></td>
                                <td style="text-align: center;"><b>Act</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $k=>$v)
                            <tr>
                                <td style="text-align: left;">{{ $v->keterangan }}</td>
                                <td style="text-align: center;">{{ $v->grade }}</td>
                                <td style="text-align: center;">{{ $v->rank }}</td>
                                <td class="bootstrap-iso">
                                    <div class="text-center">
                                        <form action="{{ route('fasilitas.destroy', $v) }}" method="post">
                                            @csrf
                                            @method('delete')

                                            <a class="btn btn-success btn-sm" onclick="editItem('{{ $v->id }}')">Edit</a>
                                            <a class="btn btn-danger btn-sm" onclick="confirm('{{ __("Hapus data?") }}') ? this.parentElement.submit() : ''">Hapus</a>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	function createItem()
	{
		location.href="/fasilitas/create";
	}

	function editItem(id)
	{
		location.href="/fasilitas/"+id+"/edit";
	}

</script>

@endsection
