@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h3>Rank Fasilitas - Input Baru</h3>
            <div class="bootstrap-iso">
                <a href="{{ URL::to('/fasilitas') }}" class="btn btn-default">Kembali</a>
            </div>
            <hr>
            <form class="form-horizontal" method="POST" action="{{ url('fasilitas') }}">
            {{ csrf_field() }}

                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-12">
                            <input class="form-control" name="keterangan" placeholder="Keterangan">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Grade</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="grade" placeholder="A / B / C">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Value/Rank</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="number" min="1" name="rank" placeholder="Value/Rank">
                        </div>
                    </div>
                </div>

                <div class="box-footer bootstrap-iso">
                    <button id="simpan" type="submit" class="btn btn-primary" hidden>Simpan Data</button>
                    <a href="javascript:cek()" class="btn btn-primary">Simpan Data</a>
                </div>

            </form>
            <hr>
        </div>
    </div>
</div>

<script type="text/javascript">

	function cek()
	{
		let a = confirm("Simpan Data?");
        if(a) {
            $('#simpan').click();
        }
	}

</script>

@endsection
