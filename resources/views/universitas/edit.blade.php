@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3>Universitas - Edit Data</h3>
            <div class="bootstrap-iso">
                <a href="{{ URL::to('/universitas') }}" class="btn btn-default">Kembali</a>
            </div>
            <hr>
            <form class="form-horizontal" method="POST" action="{{ url('universitas', $data->id) }}">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PATCH">

                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="nama" placeholder="Nama Universitas" value="{{ $data->nama }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat</label>
                        <div class="col-sm-12">
                            <input class="form-control" name="alamat" placeholder="Alamat Universitas" value="{{ $data->alamat }}">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Akreditasi</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="rank_akreditasi_id" id="rank_akreditasi">
                                @foreach (\App\RankAkreditasi::akreditasi() as $k=>$v)
                                    <option value="{{ $v->id }}">{{ $v->keterangan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Dosen</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="rank_dosen_id" id="rank_dosen">
                                @foreach (\App\RankDosen::dosen() as $k=>$v)
                                    <option value="{{ $v->id }}">{{ $v->keterangan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Fasilitas</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="rank_fasilitas_id" id="rank_fasilitas">
                                @foreach (\App\RankFasilitas::fasilitas() as $k=>$v)
                                    <option value="{{ $v->id }}">{{ $v->keterangan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Lokasi</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="rank_lokasi_id" id="rank_lokasi">
                                @foreach (\App\RankLokasi::lokasi() as $k=>$v)
                                    <option value="{{ $v->id }}">{{ $v->keterangan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>

                <div class="box-footer bootstrap-iso">
                    <button id="simpan" type="submit" class="btn btn-primary" hidden>Simpan Data</button>
                    <a href="javascript:cek()" class="btn btn-primary">Simpan Data</a>
                </div>

            </form>
            <hr>
        </div>
    </div>
</div>

<script type="text/javascript">

	function cek()
	{
		let a = confirm("Simpan Data?");
        if(a) {
            $('#simpan').click();
        }
	}

    $("#rank_akreditasi").val("{{ $data->rank_akreditasi_id }}");
    $("#rank_dosen").val("{{ $data->rank_dosen_id }}");
    $("#rank_fasilitas").val("{{ $data->rank_fasilitas_id }}");
    $("#rank_lokasi").val("{{ $data->rank_lokasi_id }}");

</script>

@endsection
