@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
	    	        <h3>Universitas</h3>
                </div>

                <div class="card-body">

                    <a class="btn btn-primary btn-md" onclick='createItem()'>Input Baru</a>
                    <hr>
                    <table id="datatable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="text-align: center;"><b>Nama</b></td>
                                <td style="text-align: center;"><b>Alamat </b></td>
                                <td style="text-align: center;"><b>Grade/Rank<br>Akreditasi</b></td>
                                <td style="text-align: center;"><b>Grade/Rank<br>Dosen</b></td>
                                <td style="text-align: center;"><b>Grade/Rank<br>Fasilitas</b></td>
                                <td style="text-align: center;"><b>Grade/Rank<br>Lokasi</b></td>
                                <td style="text-align: center;"><b>Grade/Rank<br>Keseluruhan</b></td>
                                <td style="text-align: center;"><b>Act</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $k=>$v)
                            <tr>
                                <td style="text-align: left;">{{ $v->nama }}</td>
                                <td style="text-align: left;">{{ $v->alamat }}</td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->akreditasi->keterangan) }}<br>
                                    ({{ strtoupper($v->akreditasi->grade) }} / {{ $v->akreditasi->rank }})
                                </td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->dosen->keterangan) }}<br>
                                    ({{ strtoupper($v->dosen->grade) }} / {{ $v->dosen->rank }})
                                </td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->fasilitas->keterangan) }}<br>
                                    ({{ strtoupper($v->fasilitas->grade) }} / {{ $v->fasilitas->rank }})
                                </td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->lokasi->keterangan) }}<br>
                                    ({{ strtoupper($v->lokasi->grade) }} / {{ $v->lokasi->rank }})
                                </td>
                                <td style="text-align: center;">{{ $v->rank_keseluruhan }}</td>
                                <td class="bootstrap-iso">
                                    <div class="text-center">
                                        <form action="{{ route('universitas.destroy', $v) }}" method="post">
                                            @csrf
                                            @method('delete')

                                            <a class="btn btn-success btn-sm" onclick="editItem('{{ $v->id }}')">Edit</a>
                                            <a class="btn btn-danger btn-sm" onclick="confirm('{{ __("Hapus data?") }}') ? this.parentElement.submit() : ''">Hapus</a>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	function createItem()
	{
		location.href="/universitas/create";
	}

	function editItem(id)
	{
		location.href="/universitas/"+id+"/edit";
	}

</script>

@endsection
