@extends('layouts.guest')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
	    	        <h3>Rekomensasi Universitas sesuai Parameter</h3>
                </div>

                <div class="card-body">

                    <a class="btn btn-primary btn-md" href="{{ url('proses') }}">Ganti Parameter</a>
                    <hr>
                    <table id="datatable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="text-align: center;"><b>Nama</b></td>
                                <td style="text-align: center;"><b>Alamat </b></td>
                                <td style="text-align: center;"><b>Grade/Rank<br>Akreditasi</b></td>
                                <td style="text-align: center;"><b>Grade/Rank<br>Dosen</b></td>
                                <td style="text-align: center;"><b>Grade/Rank<br>Fasilitas</b></td>
                                <td style="text-align: center;"><b>Grade/Rank<br>Lokasi</b></td>
                                <td style="text-align: center;"><b>Grade/Rank<br>Keseluruhan</b></td>
                                <td style="text-align: center;"><b>Pendekatan<br>Penilaian</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($rekomendasi as $k=>$v)
                            <tr>
                                <td style="text-align: left;">{{ $v->nama }}</td>
                                <td style="text-align: left;">{{ $v->alamat }}</td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->akreditasi->keterangan) }}<br>
                                    ({{ strtoupper($v->akreditasi->grade) }} / {{ $v->akreditasi->rank }})
                                </td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->dosen->keterangan) }}<br>
                                    ({{ strtoupper($v->dosen->grade) }} / {{ $v->dosen->rank }})
                                </td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->fasilitas->keterangan) }}<br>
                                    ({{ strtoupper($v->fasilitas->grade) }} / {{ $v->fasilitas->rank }})
                                </td>
                                <td style="text-align: center;">
                                    {{ strtoupper($v->lokasi->keterangan) }}<br>
                                    ({{ strtoupper($v->lokasi->grade) }} / {{ $v->lokasi->rank }})
                                </td>
                                <td style="text-align: center;">{{ $v->rank_keseluruhan }}</td>
                                <td style="text-align: center;">{{ 100 - $v->pendekatan }}%</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
