@extends('layouts.guest')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12" style="background-color: lightsteelblue;">
            <form class="form-horizontal" method="POST" action="{{ url('proses') }}">
            {{ csrf_field() }}

                <div class="box-body">
                    <hr>
                    <h3 class="text-center text-success">
                        <b>Input Identitas Diri</b>
                    </h3>
                    <hr>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="nama" placeholder="Nama Calon Mahasiswa" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="email" name="email" placeholder="Email Calon Mahasiswa" required>
                        </div>
                    </div>

                    <hr>
                    <hr>
                    <h3 class="text-center text-success">
                        <b>Input Parameter SPK</b>
                        <br><b>yang diinginkan</b>
                    </h3>
                    <hr>

                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <label class="col-sm-3 control-label">Akreditasi</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="rank_akreditasi_id" id="rank_akreditasi">
                                @foreach (\App\RankAkreditasi::akreditasi() as $k=>$v)
                                    <option value="{{ $v->id }}">{{ $v->keterangan }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <label class="col-sm-3 control-label">Dosen</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="rank_dosen_id" id="rank_dosen">
                                @foreach (\App\RankDosen::dosen() as $k=>$v)
                                    <option value="{{ $v->id }}">{{ $v->keterangan }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <label class="col-sm-3 control-label">Fasilitas</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="rank_fasilitas_id" id="rank_fasilitas">
                                @foreach (\App\RankFasilitas::fasilitas() as $k=>$v)
                                    <option value="{{ $v->id }}">{{ $v->keterangan }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <label class="col-sm-3 control-label">Lokasi</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="rank_lokasi_id" id="rank_lokasi">
                                @foreach (\App\RankLokasi::lokasi() as $k=>$v)
                                    <option value="{{ $v->id }}">{{ $v->keterangan }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>

                </div>

                <div class="box-footer text-center">
                    <button id="simpan" type="submit" class="btn btn-primary hidden"></button>
                    <a href="javascript:cek()" class="btn btn-primary">Submit</a>
                </div>

            </form>
            <hr>
        </div>
    </div>
</div>

<script type="text/javascript">

	function cek()
	{
		let a = confirm("Proses Data?");
        if(a) {
            $('#simpan').click();
        }
	}

</script>

@endsection
