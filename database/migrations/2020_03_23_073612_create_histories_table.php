<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->id();
                $table->string('nama', 100)->nullable();
                $table->string('email', 100)->nullable();
                $table->tinyInteger('rank_lokasi_id')->nullable();
                $table->tinyInteger('rank_fasilitas_id')->nullable();
                $table->tinyInteger('rank_dosen_id')->nullable();
                $table->tinyInteger('rank_akreditasi_id')->nullable();
                $table->tinyInteger('rank_keseluruhan')->nullable();
                $table->integer('universitas_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
