<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUniversitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universitas', function (Blueprint $table) {
            $table->id();
                $table->string('nama', 100)->nullable();
                $table->string('alamat', 100)->nullable();
                $table->tinyInteger('rank_lokasi_id')->nullable();
                $table->tinyInteger('rank_fasilitas_id')->nullable();
                $table->tinyInteger('rank_dosen_id')->nullable();
                $table->tinyInteger('rank_akreditasi_id')->nullable();
                $table->tinyInteger('rank_keseluruhan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universitas');
    }
}
