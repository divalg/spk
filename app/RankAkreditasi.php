<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankAkreditasi extends Model
{
    protected $fillable = [
        'keterangan',
        'grade',
        'rank',
    ];

    public static function akreditasi()
    {
        return RankAkreditasi::orderBy('grade')->get();
    }
}
