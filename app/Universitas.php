<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Universitas extends Model
{
    protected $fillable = [
        'nama',
        'alamat',
        'rank_akreditasi_id',
        'rank_dosen_id',
        'rank_fasilitas_id',
        'rank_lokasi_id',
        'rank_keseluruhan',
    ];

    public function akreditasi()
    {
        return $this->belongsTo('\App\RankAkreditasi', 'rank_akreditasi_id');
    }

    public function dosen()
    {
        return $this->belongsTo('\App\RankDosen', 'rank_dosen_id');
    }

    public function fasilitas()
    {
        return $this->belongsTo('\App\RankFasilitas', 'rank_fasilitas_id');
    }

    public function lokasi()
    {
        return $this->belongsTo('\App\RankLokasi', 'rank_lokasi_id');
    }
}
