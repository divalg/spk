<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankLokasi extends Model
{
    protected $fillable = [
        'keterangan',
        'grade',
        'rank',
    ];

    public static function lokasi()
    {
        return RankLokasi::orderBy('grade')->get();
    }
}
