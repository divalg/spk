<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankDosen extends Model
{
    protected $fillable = [
        'keterangan',
        'grade',
        'rank',
    ];

    public static function dosen()
    {
        return RankDosen::orderBy('grade')->get();
    }
}
