<?php

namespace App\Http\Controllers;

use App\RankDosen;
use App\RankLokasi;
use App\Universitas;
use App\RankFasilitas;
use App\RankAkreditasi;
use Illuminate\Http\Request;

class UniversitasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Universitas::orderBy('nama')->get();
        return view('universitas.index', compact('data'));
    }

    public function create()
    {
        return view('universitas.create');
    }

    public function store(Request $r)
    {
        $data = $r->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'rank_akreditasi_id' => 'required|numeric',
            'rank_dosen_id' => 'required|numeric',
            'rank_fasilitas_id' => 'required|numeric',
            'rank_lokasi_id' => 'required|numeric',
        ]);

        $a = Universitas::make($data);

        $akreditasi = RankAkreditasi::find($r->rank_akreditasi_id)->rank;
        $dosen      = RankDosen::find($r->rank_dosen_id)->rank;
        $fasilitas  = RankFasilitas::find($r->rank_fasilitas_id)->rank;
        $lokasi     = RankLokasi::find($r->rank_lokasi_id)->rank;

        $a->rank_keseluruhan = $akreditasi + $dosen + $fasilitas + $lokasi;
        $a->save();

        return redirect('universitas');
    }

    public function edit($id)
    {
        $data = Universitas::find($id);
        return view('universitas.edit', compact('data'));
    }

    public function update(Request $r, $id)
    {
        $data = $r->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'rank_akreditasi_id' => 'required|numeric',
            'rank_dosen_id' => 'required|numeric',
            'rank_fasilitas_id' => 'required|numeric',
            'rank_lokasi_id' => 'required|numeric',
        ]);

        $a = Universitas::find($id);
        $a->update($data);

        $akreditasi = RankAkreditasi::find($r->rank_akreditasi_id)->rank;
        $dosen      = RankDosen::find($r->rank_dosen_id)->rank;
        $fasilitas  = RankFasilitas::find($r->rank_fasilitas_id)->rank;
        $lokasi     = RankLokasi::find($r->rank_lokasi_id)->rank;

        $a->rank_keseluruhan = $akreditasi + $dosen + $fasilitas + $lokasi;
        $a->save();

        return redirect('universitas');
    }

    public function destroy($id)
    {
        $a = Universitas::destroy($id);
        return redirect('universitas');
    }

}
