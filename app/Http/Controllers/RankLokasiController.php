<?php

namespace App\Http\Controllers;

use App\RankLokasi;
use Illuminate\Http\Request;

class RankLokasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = RankLokasi::orderBy('grade')->get();
        return view('lokasi.index', compact('data'));
    }

    public function create()
    {
        return view('lokasi.create');
    }

    public function store(Request $r)
    {
        $data = $r->validate([
            'keterangan' => 'required',
            'grade' => 'required',
            'rank' => 'required'
        ]);

        $a = RankLokasi::make($data);
        $a->save();

        return redirect('lokasi');
    }

    public function edit($id)
    {
        $data = RankLokasi::find($id);
        return view('lokasi.edit', compact('data'));
    }

    public function update(Request $r, $id)
    {
        $data = $r->validate([
            'keterangan' => 'required',
            'grade' => 'required',
            'rank' => 'required'
        ]);

        $a = RankLokasi::find($id)->update($data);

        return redirect('lokasi');
    }

    public function destroy($id)
    {
        $a = RankLokasi::destroy($id);
        return redirect('lokasi');
    }

}
