<?php
namespace App\Http\Controllers;

use App\History;
use App\RankDosen;
use App\RankLokasi;
use App\Universitas;
use App\RankFasilitas;
use App\RankAkreditasi;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;

class SpkController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    private function calculateAHP($v, $r) {
        $x = $this->getBobot($r) - $v->rank_keseluruhan;
        if ($x < 0) {
            $x *= -1;
        }
        return $x;
    }

    private function getBobot($r) {
        $akreditasi = RankAkreditasi::find($r->rank_akreditasi_id)->rank;
        $dosen      = RankDosen::find($r->rank_dosen_id)->rank;
        $fasilitas  = RankFasilitas::find($r->rank_fasilitas_id)->rank;
        $lokasi     = RankLokasi::find($r->rank_lokasi_id)->rank;
        $bobot      = $akreditasi + $dosen + $fasilitas + $lokasi;
        return $bobot;
    }

    public function store(Request $r)
    {
        $data = $r->validate([
            'nama' => 'required',
            'email' => 'required|email',
            'rank_akreditasi_id' => 'required',
            'rank_dosen_id' => 'required',
            'rank_fasilitas_id' => 'required',
            'rank_lokasi_id' => 'required',
        ]);

        $universitas = Universitas::orderBy('rank_keseluruhan')->get();

        $rekomendasi = [];
        foreach($universitas as $k=>$v) {
            $v->pendekatan = $this->calculateAHP($v, $r);
            $rekomendasi[] = $v;
        }

        $rekomendasi = Arr::sort($rekomendasi, function($data) {
            return $data->pendekatan;
        });

        $universitas_id = 0;
        foreach($rekomendasi as $k=>$v) {
            $universitas_id = $v->id; break;
        }

        $hasil = History::where('email', $r->email)->first();
        if (isset($hasil)) {
            $hasil->update($data);
            $hasil->save;
        } else {
            $hasil = History::make($data);
        }
        $hasil->rank_keseluruhan = $this->getBobot($r);
        $hasil->universitas_id = $universitas_id;
        $hasil->save();

        return view('spk.hasil', compact('hasil', 'rekomendasi'));
    }

    public function index()
    {
        return view('spk.index');
    }

}
