<?php
namespace App\Http\Controllers;

use App\RankAkreditasi;
use Illuminate\Http\Request;

class RankAkreditasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = RankAkreditasi::orderBy('grade')->get();
        return view('akreditasi.index', compact('data'));
    }

    public function create()
    {
        return view('akreditasi.create');
    }

    public function store(Request $r)
    {
        $data = $r->validate([
            'keterangan' => 'required',
            'grade' => 'required',
            'rank' => 'required'
        ]);

        $a = RankAkreditasi::make($data);
        $a->save();

        return redirect('akreditasi');
    }

    public function edit($id)
    {
        $data = RankAkreditasi::find($id);
        return view('akreditasi.edit', compact('data'));
    }

    public function update(Request $r, $id)
    {
        $data = $r->validate([
            'keterangan' => 'required',
            'grade' => 'required',
            'rank' => 'required'
        ]);

        $a = RankAkreditasi::find($id)->update($data);

        return redirect('akreditasi');
    }

    public function destroy($id)
    {
        $a = RankAkreditasi::destroy($id);
        return redirect('akreditasi');
    }

}
