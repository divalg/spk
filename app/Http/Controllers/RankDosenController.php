<?php

namespace App\Http\Controllers;

use App\RankDosen;
use Illuminate\Http\Request;

class RankDosenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = RankDosen::orderBy('grade')->get();
        return view('dosen.index', compact('data'));
    }

    public function create()
    {
        return view('dosen.create');
    }

    public function store(Request $r)
    {
        $data = $r->validate([
            'keterangan' => 'required',
            'grade' => 'required',
            'rank' => 'required'
        ]);

        $a = RankDosen::make($data);
        $a->save();

        return redirect('dosen');
    }

    public function edit($id)
    {
        $data = RankDosen::find($id);
        return view('dosen.edit', compact('data'));
    }

    public function update(Request $r, $id)
    {
        $data = $r->validate([
            'keterangan' => 'required',
            'grade' => 'required',
            'rank' => 'required'
        ]);

        $a = RankDosen::find($id)->update($data);

        return redirect('dosen');
    }

    public function destroy($id)
    {
        $a = RankDosen::destroy($id);
        return redirect('dosen');
    }

}
