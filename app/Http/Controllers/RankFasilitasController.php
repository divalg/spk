<?php

namespace App\Http\Controllers;

use App\RankFasilitas;
use Illuminate\Http\Request;

class RankFasilitasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = RankFasilitas::orderBy('grade')->get();
        return view('fasilitas.index', compact('data'));
    }

    public function create()
    {
        return view('fasilitas.create');
    }

    public function store(Request $r)
    {
        $data = $r->validate([
            'keterangan' => 'required',
            'grade' => 'required',
            'rank' => 'required'
        ]);

        $a = RankFasilitas::make($data);
        $a->save();

        return redirect('fasilitas');
    }

    public function edit($id)
    {
        $data = RankFasilitas::find($id);
        return view('fasilitas.edit', compact('data'));
    }

    public function update(Request $r, $id)
    {
        $data = $r->validate([
            'keterangan' => 'required',
            'grade' => 'required',
            'rank' => 'required'
        ]);

        $a = RankFasilitas::find($id)->update($data);

        return redirect('fasilitas');
    }

    public function destroy($id)
    {
        $a = RankFasilitas::destroy($id);
        return redirect('fasilitas');
    }

}
