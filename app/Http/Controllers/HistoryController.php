<?php
namespace App\Http\Controllers;

use App\History;

class HistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = History::all();
        return view('history.index', compact('data'));
    }

}
