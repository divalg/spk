<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankFasilitas extends Model
{
    protected $fillable = [
        'keterangan',
        'grade',
        'rank',
    ];

    public static function fasilitas()
    {
        return RankFasilitas::orderBy('grade')->get();
    }
}
